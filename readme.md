## ArabicTextNormalizer
Simple package to normalize arabic text to prepare it for further text processing.

### Installation
```
composer require uthman/arabic-text-normalizer
```

### Usage

Use the package by initializing an instance of `ArabicTextNormalizer` class, then chain any number of available methods _mentioned below_ to it as per your need, lastely call the `normalize()` method on it.

```php
use UthmanArabicTextNormalizerArabicTextNormalizer;

$normalizer = (new ArabicTextNormalizer('1ش2ا3ه4د5ن6٧ا٨ ٩الً٤نُ١تٌ٢ي٣ــــــجة'))
    ->stripKasheeda()
    ->stripTashkeel()
    ->stripNumbers();

echo $normalizer->normalize();
// شاهدنا النتيجة
```

### Available Methods
- `stripNonArabic()`: Strip non arabic characters.
- `stripKasheeda()`: Strip the Kasheeda symbol `ـ`.
- `stripSymbols()`: Strip large set of symbols including ``!"#$%&'()*+,-./\:;<=>?؟@[]^_`{|}~`÷×؛،,٪”“``.
- `stripTashkeel()`: Strip Arabic tashkeel diacritics `َ`, `ً`, `ُ`, `ٌ`, `ِ`, `ٍ`, `~`, `ْ`, `ّ`.
- `stripNumbers()`: Strip both Arabic and English digits characters `0123456789٠١٢٣٤٥٦٧٨٩`.
- `normalizeWhitespace()`: Normalize whitespace by replacing multi consecuitive spaces by one space.
- `normalizeAlef()`: Normalize different shapes of Alef `أإآ` into `ا`.
- `normalizeHaa()`: Normalize Haa `ه` character into Taa `ة`.
- `normalizeTaa()`: Normalize Taa `ة` character into Haa `ه`.
