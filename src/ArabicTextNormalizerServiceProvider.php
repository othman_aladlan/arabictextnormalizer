<?php

namespace Uthman\ArabicTextNormalizer;

use Illuminate\Support\ServiceProvider;

class ArabicTextNormalizerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot()
    {
        //
    }
}
