<?php

namespace Uthman\ArabicTextNormalizer;

class ArabicTextNormalizer
{
    protected $str;

    public function __construct(string $str)
    {
        $this->str = $str;
    }

    public function normalize() :string
    {
        return trim($this->str);
    }

    public function stripNonArabic()
    {
        $this->str = preg_replace('/[^\p{Arabic}\d\-\s]/ui', '', $this->str);

        return $this;
    }

    public function stripKasheeda()
    {
        $this->str = preg_replace('/ـ/', '', $this->str);

        return $this;
    }

    public function stripSymbols()
    {
        $this->str = str_replace('\\', '', $this->str);
        $this->str = preg_replace($this->symbolsPattern(), '', $this->str);

        return $this;
    }

    public function stripTashkeel()
    {
        $this->str = preg_replace('/[ًٌٍَُِّ~ْ]+/ui', '', $this->str);

        return $this;
    }

    public function stripNumbers()
    {
        $this->str = preg_replace('/[0123456789٠١٢٣٤٥٦٧٨٩]+/ui', '', $this->str);

        return $this;
    }

    public function normalizeWhitespace()
    {
        $this->str = preg_replace('/\s+/', ' ', $this->str);

        return $this;
    }

    public function normalizeAlef()
    {
        $this->str = preg_replace('/[أإآ]+/ui', 'ا', $this->str);
        // TODO: config it

        return $this;
    }

    public function normalizeHaa()
    {
        $this->str = preg_replace('/ه+/ui', 'ة', $this->str);

        return $this;
    }

    public function normalizeTaa()
    {
        $this->str = preg_replace('/ة+/ui', 'ه', $this->str);

        return $this;
    }

    protected function symbolsPattern() :string
    {
        return '/[\!\"\#\$\%\&\'\(\)\*\+\,\-\.\/\:\;\<\=\>\?\؟\@\[\]\^\_\`\{\|\}\~\`\÷\×\؛\،\,\٪\”\“]+/ui';
    }

    public function __toString()
    {
        try {
            return (string) $this->str;
        } catch (\Exception $e) {
            return '';
        }
    }
}
